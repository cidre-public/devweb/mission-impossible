/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.agents;

import javax.ejb.Remote;

/**
 * Interface distante pour la classe Account.
 * Actuellement non utilisée. Si vous arrivez à accéder de manière distante 
 * à cet EJB, faites-moi signe.
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Remote
public interface AccountRemote {

    /**
     * Récupération du login de l'utilisateur actuellement connecté.
     * @return Le login de l'utilisateur connecté.
     */
    public String getUsername();
    
}
