/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.managed;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Named;
import mi.missions.ListeMissions;
import mi.missions.Mission;
import mi.missions.TraitementMissionsRemote;

/**
 * "Managed bean" pour accéder à la liste des missions.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Named("missionListController")
public class MissionListController {
    //TODO Il manquera très certainement des méthodes dans cette classe.
    
    /**
     * EJB encapsulant toutes les missions connues du système.
     */
    @EJB
    private ListeMissions missions;
    
    /**
     * EJB proposant l'API de traitement des missions.
     */
    @EJB
    private TraitementMissionsRemote tm;

    /**
     * Constructeur par défaut. 
     */
    public MissionListController() {
    }
    
    /**
     * Récupération de l'ensemble des missions.
     * @return Liste de toutes les missions connues du système.
     */
    public ArrayList<Mission> getMissions() {
        return this.missions.getMissions();
    }
    
    /**
     * Introduction d'une nouvelle mission dans le système.
     * @param m La mission à introduire dans le système (elle pourra être ou ne 
     * pas être clonée).
     */
    public void creerMission(Mission m) {
        //TODO À compléter !
    }
    
}
