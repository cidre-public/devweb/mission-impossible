/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.managed;

import mi.agents.AccountRemote;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * "Managed bean" pour gérer un compte utilisateur.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Named("accountController")
public class AccountController implements Serializable {
    
    /**
     * Juste pour éviter un warning.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * EJB encapsulant le compte de l'utilisateur connecté.
     */
    @EJB
    private AccountRemote account;

    /**
     * Constructeur par défaut.
     */
    public AccountController() {
    }
    
    /**
     * Récupération du login de l'utilisateur actuellement connecté.
     * @return Le login de l'utilisateur.
     */
    public String getUsername() {
        return account.getUsername();
    }
    
    /**
     * Déconnexion de l'utilisateur.
     * @return "Outcome" de navigation correspondant à un rechargement de la 
     * page d'accueil, pour prendre en compte la déconnexion.
     */
    public String logout() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc != null) {
            ExternalContext ec = fc.getExternalContext();
            if (ec != null) {
                ec.invalidateSession(); // Lance un NPE sans conséquence : bug Glassfish, apparemment (https://java.net/jira/browse/GLASSFISH-21166)
            }
        }
        return "/index.xhtml?faces-redirect=true";
    }
    
}
