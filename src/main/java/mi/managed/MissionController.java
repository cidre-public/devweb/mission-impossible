/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.managed;

import mi.agents.AccountRemote;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import mi.missions.Mission;
import mi.missions.TraitementMissionsRemote;

/**
 * "Managed bean" pour accéder à une mission.
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Named("missionController")
public class MissionController implements Serializable {
    //TODO Il manquera très certainement des méthodes dans cette classe.
    
    /**
     * Juste pour éviter un warning.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * La mission à laquelle il est fait référence.
     * Elle n'est pas forcément déjà connue du système.
     */
    private Mission mission;
    
    /**
     * EJB proposant l'API de traitement des missions.
     */
    @EJB
    private TraitementMissionsRemote tm;
    
    /**
     * EJB encapsulant le compte de l'utilisateur connecté.
     */
    @EJB
    private AccountRemote account;

    /**
     * Constructeur par défaut.
     */
    public MissionController() {
    }
    
    /**
     * Méthode d'initialisation.
     * On crée une nouvelle mission vierge, que l'on déclare comme ayant été
     * demandée par l'utilisateur actuellement connecté. Il est impossible de 
     * faire ça dans le constructeur, parce que les EJB sont injectés après.
     */
    @PostConstruct
    private void init() {
        this.mission = new Mission();
        if (this.account == null) {
            // Ce n'est pas forcément une erreur, qui sait.
            System.err.println("Pas moyen d'avoir un compte !");
        }
        if (this.account != null) {
            this.mission.setAgent(this.account.getUsername());
        }
    }
    
    /**
     * Récupération de l'objet mission.
     * @return L'objet mission.
     */
    public Mission getMission() {
        return this.mission;
    }
    
    /**
     * Récupération du lieu de la mission.
     * @return Le nom du lieu de la mission.
     */
    public String getLieu() {
        return this.mission.getLieu();
    }
    
    /**
     * Récupération de la date de la mission
     * @return Représentation textuelle de la date au format "dd/mm/yyyy".
     */
    public String getDate() {
        return this.mission.getDate();
    }
    
    /**
     * Définition de la date de la mission.
     * @param dateString Représentation textuelle de la date au format "dd/mm/yyyy".
     */
    public void setDate(String dateString) {
        // J'ai été sympa, je vous ai épargné cinq minutes de Google.
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            this.mission.setDate(formatter.parse(dateString));
        } catch (ParseException ex) {
            Logger.getLogger(MissionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Méthode de test de l'EJB de traitement des missions.
     * @return Résultat du test.
     */
    public String testTM() {
        return this.tm.test();
    }
    
}
