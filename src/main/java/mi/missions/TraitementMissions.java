/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package mi.missions;

import mi.agents.AccountRemote;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * EJB proposant l'API de traitement des missions.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Stateless
public class TraitementMissions implements TraitementMissionsRemote {
    
    /**
     * EJB encapsulant le compte de l'utilisateur connecté.
     */
    @EJB
    private AccountRemote account;
    
    /**
     * EJB encapsulant toutes les missions connues du système.
     */
    @EJB
    private ListeMissions missions;
    
    @Override
    public void attribuerMission(int missionID) {
        //TODO À compléter !
    }

    @Override
    public ArrayList<Mission> getMissions() {
        return this.missions.getMissions();
    }
    
    @Override
    public String test() {
        return "Test depuis l'EJB : " + this.account.getUsername();
    }

    @Override
    public void valider(int missionID) {
        //TODO À compléter !
    }

    @Override
    public void refuser(int missionID) {
        //TODO À compléter !
    }

    @Override
    public void payer(int missionID) {
        //TODO À compléter !
    }

    @Override
    public void frais(int missionID) {
        //TODO À compléter !
    }

    @Override
    public String getUsername() {
        return this.account.getUsername();
    }
}
