/**
 * BE Java EE "Mission Impossible" Mineure "développement web" CentraleSupélec,
 * campus de Rennes
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr> 2016
 */
package mi.missions;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Java Bean représentant une mission.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
public class Mission implements Serializable {

    /**
     * Juste pour éviter un warning.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Générateur de nombres pseudo-aléatoires.
     */
    private static final Random generator = new Random();

    //TODO pour une version ultérieure du BE, 
    // s'assurer de la correction des accès concurrents.
    /**
     * État de la mission.
     */
    private EtatMission etat;

    /**
     * Lieu de la mission.
     */
    private String lieu;

    /**
     * Date de la mission (granularité = jour).
     */
    private Date date;

    /**
     * Identifiant unique (ID) de la mission.
     */
    private final int id;

    /**
     * Login de l'agent qui doit partir en mission.
     */
    private String agent;

    /**
     * Login de l'agent du service adminstratif qui traite la mission.
     */
    private String agentAdmin;

    /**
     * Constructeur par défaut : mission "creuse", dans l'état DEMANDEE avec un
     * identifiant unique.
     */
    public Mission() {
        this.etat = EtatMission.DEMANDEE;
        this.id = generator.nextInt();
    }

    /**
     * Constructeur complet, utilisé pour la génération des missions de test.
     *
     * @param etat État de la mission.
     * @param lieu Lieu de la mission.
     * @param date Date de la mission.
     * @param agent Login de l'agent partant en mission.
     * @param agentAdmin Login de l'agent administratif suivant la mission.
     */
    public Mission(EtatMission etat, String lieu, Date date, String agent, String agentAdmin) {
        this.etat = etat;
        this.lieu = lieu;
        this.date = (Date) date.clone();
        this.agent = agent;
        this.agentAdmin = agentAdmin;
        this.id = generator.nextInt();
    }

    /**
     * Récupération de l'état de la mission.
     *
     * @return L'état de la mission.
     */
    public EtatMission getEtat() {
        return this.etat;
    }

    /**
     * Récupération du lieu de la mission.
     *
     * @return Le lieu de la mission.
     */
    public String getLieu() {
        return this.lieu;
    }

    /**
     * Définition du lieu de la mission.
     *
     * @param lieu Le lieu de la mission.
     */
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    /**
     * Récupération de l'identifiant unique de la mission.
     *
     * @return L'identifiant unique de la mission.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Récupération de la date de la mission.
     *
     * @return Représentation textuelle de la date au format "dd/mm/yyyy".
     */
    public String getDate() {
        if (this.date != null) {
            return new SimpleDateFormat("dd/MM/yyyy").format(this.date);
        } else {
            return "";
        }
    }

    /**
     * Définition de la date de la mission.
     *
     * @param d La date de la mission.
     */
    public void setDate(Date d) {
        this.date = (Date) d.clone();
    }

    /**
     * Récupération de l'agent partant en mission.
     *
     * @return Le login de l'agent.
     */
    public String getAgent() {
        return this.agent;
    }

    /**
     * Définition de l'agent partant en mission.
     *
     * @param agent Le login de l'agent.
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * Définition de l'agent administratif gérant la mission.
     *
     * @param agentAdmin Le login de l'agent administratif.
     */
    public void setAgentAdmin(String agentAdmin) {
        this.agentAdmin = agentAdmin;
        System.err.println("nouvel agent admin : " + agentAdmin);
    }

    /**
     * Récupération de l'agent administratif gérant la mission.
     *
     * @return Le login de l'agent administratif.
     */
    public String getAgentAdmin() {
        return this.agentAdmin;
    }

    /**
     * Définition de l'état de la mission.
     *
     * @param etat L'état de la mission.
     */
    public void setEtat(EtatMission etat) {
        this.etat = etat;
    }

    /**
     * Représentation textuelle d'une mission. Éventuellement utile à des fins
     * de débogage.
     *
     * @return Représentation textuelle incluant tout les éléments de la
     * mission, y compris un hash caractérisant son emplacement en mémoire (i.e.
     * deux instances dont l'une est un clone de l'autre auront des hachés
     * différents).
     */
    @Override
    public String toString() {
        return "Mission(" + this.lieu + ", " + this.agent + ", " + this.agentAdmin
                + ", " + this.etat + ", " + this.getDate() + ", " + this.id + ", " + this.hashCode() + ")";
    }

}
